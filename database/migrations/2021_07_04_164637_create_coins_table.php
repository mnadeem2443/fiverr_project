<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->id();
            $table->string('userid');
            $table->string('coin_name');
            $table->string('symbol');
            $table->string('logo');
            $table->integer('price');
            $table->integer('market_cap');
            $table->date('launch_date');
            $table->string('binance_smart_chain');
            $table->string('ethereum');
            $table->string('solana');
            $table->string('website');
            $table->string('telegram');
            $table->string('twitter');
            $table->string('additional');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins');
    }
}
