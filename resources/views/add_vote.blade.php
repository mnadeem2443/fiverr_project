@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add Vote') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (isset($OK))
                        @if ($OK == "OK")
                        <div class="alert alert-success" role="alert">
                            New Coin Added Successfuly
                        </div>
                        @endif
                    @endif

                    <form method="POST" action="{{route('add_vote')}}" >
                        @csrf
                        <input type="hidden" name="userid" value="{{auth::getUser()->id}}">
                        <input type="hidden" name="coinid" value="{{$coin->id}}">
                        <h3>Coin Informations</h3>
                        <div class="form-group">
                            <label for="coin_name">Name</label>
                            <input class="form-control" type="text" name="coin_name" id="coin_name" value="{{ $coin->coin_name }}" disabled>
                        </div>
                        
                        <div class="form-group">
                            <label for="symbol">Symbol</label>
                            <input class="form-control" id="Symbol" type="text" name="symbol" value="{{ $coin->symbol }}" disabled>
                        </div>
                        
                        <input type="submit" name="submit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
