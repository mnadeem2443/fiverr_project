@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>Promoted coins</h3>
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif

            @if (isset($data))
            <table class='table bg-white rounded table-hover'>
                @if($data->count() > 0)
                @foreach($data as $coin)
                <tr>
                    <td><!-- Coin logo -->
                        <a href="{{ route('coin', ['id' => $coin->id]) }}"><img src="{{ $coin->logo }}" style="width: 40px; height: 40px;"></a>
                    </td>
                    <td><a href="{{ route('coin', ['id' => $coin->id]) }}">{{ $coin->coin_name }}</a></td> <!-- Coin Name -->
                    <td><a href="{{ route('coin', ['id' => $coin->id]) }}">Diff %</a></td> <!--  -->
                    <td><a href="{{ route('coin', ['id' => $coin->id]) }}">{{__("$")}}{{ $coin->market_cap }}</a></td> <!-- Value -->
                    <td><a href="{{ route('coin', ['id' => $coin->id]) }}">{{ $coin->launch_date }}</a></td> <!-- days to be lunched -->
                    <td>
                        <a href="{{ route('vote', ['id' => $coin->id]) }}">
                            <button class='btn border-success text-success'>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                            </svg>
                            {{ $vote->where('coinid',$coin->id)->count() }}
                        </button>
                    </a>
                    </td>
                </tr>
                @endforeach
                @endif
            </table>
            @endif

            <ul class="nav nav-pills">
                <li>
                    <button data-toggle="pill" href="#home" class="btn text-success">All Time Best</button>
                </li>
                <li>
                    <button data-toggle="pill" href="#menu1" class="btn text-success">Today's best</button>
                </li>
                <li>
                    <button data-toggle="pill" href="#menu2" class="btn text-success">Your Hunt</button>
                </li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <table class='table bg-white rounded table-hover'>
                        <tr>
                            <th>Search</th>
                            <th></th>
                            <th>24h</th>
                            <th>Market cap</th>
                            <th>Time since Launch</th>
                            <th>Votes</th>
                        </tr>

                        <!-- 1 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 2 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 3 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 4 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 5 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                 <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                </svg>
                                1066
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="menu1" class="tab-pane fade">
                <table class='table bg-white rounded table-hover'>
                        <tr>
                            <th>Search</th>
                            <th></th>
                            <th>24h</th>
                            <th>Market cap</th>
                            <th>Time since Launch</th>
                            <th>Votes</th>
                        </tr>

                        <!-- 1 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 2 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 3 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 4 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 5 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                 <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                </svg>
                                1066
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="menu2" class="tab-pane fade">
                <table class='table bg-white rounded table-hover'>
                        <tr>
                            <th>Search</th>
                            <th></th>
                            <th>24h</th>
                            <th>Market cap</th>
                            <th>Time since Launch</th>
                            <th>Votes</th>
                        </tr>

                        <!-- 1 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 2 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 3 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 4 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                    </svg>
                                    1066
                                </button>
                            </td>
                        </tr>

                        <!-- 5 -->
                        <tr>
                            <td><!-- Coin logo -->
                                <img src="{{ asset('img/1.png') }}" style="width: 40px; height: 40px;">
                            </td>
                            <td>Floki Inu</td> <!-- Coin Name -->
                            <td>Diff %</td> <!--  -->
                            <td>$500</td> <!-- Value -->
                            <td>Launch in 5 Days</td> <!-- days to be lunched -->
                            <td>
                                <button class='btn border-success text-success'>
                                 <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                                </svg>
                                1066
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
