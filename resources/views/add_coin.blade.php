@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Coin Listing request') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (isset($OK))
                        @if ($OK == "OK")
                        <div class="alert alert-success" role="alert">
                            New Coin Added Successfuly
                        </div>
                        @endif
                    @endif

                    <form method="POST" action="{{route('add_coin')}}" >
                        @csrf
                        <input type="hidden" name="userid" value="{{auth::getUser()->id}}">
                        <h3>Coin Informations</h3>
                        <div class="form-group">
                            <label for="coin_name">Name</label>
                            <input class="form-control" type="text" name="coin_name" id="coin_name" required>
                        </div>
                        
                        <div class="form-group">
                            <label for="symbol">Symbol</label>
                            <input class="form-control" id="Symbol" type="text" name="symbol" required>
                        </div>

                        <div class="form-group">
                            <label for="logo">Logo</label>
                            <input class="form-control" id="logo" type="text" name="logo" >
                        </div>
                        
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input class="form-control" id="price" type="number" name="price" required>    
                        </div>
                        
                        <div class="form-group">
                            <label for="market_cap">Market Cap</label>
                            <input class="form-control" id="market_cap" type="number" name="market_cap" required>
                        </div>
                        
                        <div class="form-group">
                            <label for="launch_date">Launch Date</label>
                            <input class="form-control" id="launch_date" type="date" name="launch_date" required>
                        </div>
                        

                        <h3>Coin Contracts</h3>
                        <div class="form-group">
                            <label for="binance_smart_chain">Binance Smart Chain</label>
                            <input class="form-control" id="binance_smart_chain" type="text" name="binance_smart_chain">
                        </div>
                        
                        <div class="form-group">
                            <label for="ethereum">Ethereum</label>
                            <input class="form-control" id="ethereum" type="text" name="ethereum" >    
                        </div>
                        
                        <div class="form-group">
                            <label for="solana">Solana</label>
                            <input class="form-control" id="solana" type="text" name="solana">    
                        </div>
                        
                        <h3>Coin Links</h3>
                        <div class="form-group">
                            <label for="website">Website</label>
                            <input class="form-control" id="website" type="text" name="website" required>    
                        </div>
                        
                        <div class="form-group">
                            <label for="telegram">Telegram</label>
                            <input class="form-control" id="telegram" type="text" name="telegram">    
                        </div>
                        
                        <div class="form-group">
                            <label for="twitter">Twitter</label>
                            <input class="form-control" id="twitter" type="text" name="twitter">    
                        </div>
                        
                        <h3>Additional Information</h3>
                        <div class="form-group">
                            <textarea class="form-control" id="additional" name="additional"></textarea>    
                        </div>
                        
                        <input type="submit" name="submit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
