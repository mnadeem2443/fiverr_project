@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 bg-white rounded shadow">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{$coin->logo}}" style="width:100px; height:100px">
                </div>
                <div class="col-md-8">
                    <h3>
                        {{ $coin->coin_name }}
                        <span class="bg-secondary">{{ $coin->symbol }}</span>
                    </h3>
                    <p>Binance Smart Chain:{{ $coin->binance_smart_chain }}</p>
                </div>
            </div>
            <p>
                {{ $coin->additional }}
            </p>
        </div>
            
        <span class="col-md-1"></span>
        <div class="col-md-3">
            <div class="bg-white rounded shadow p-2">
                <h3>Price</h3>
                <p>{{ $coin->price }}</p>

                <h3>Market Cap</h3>
                <p>{{ $coin->market_cap }}</p>

                <h3>Launch Date</h3>
                <p>{{ $coin->launch_date }}</p>    
            </div>
            <span class="m-1">
                
            </span>
            <div class="bg-white rounded shadow p-2">
                <a href="{{ $coin->telegram }}"><button class="btn btn-success form-control m-1">Telegram</button></a>
                <a href="{{ $coin->twitter }}"><button class="btn btn-success form-control m-1">Twitter</button></a>
                <a href="{{ $coin->website }}"><button class="btn btn-success form-control m-1">Website</button></a>
            </div>
        </div>
    </div>
</div>
@endsection
