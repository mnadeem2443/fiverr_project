<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/add_coin', [App\Http\Controllers\CoinController::class, 'create'])->name('add_coin')->middleware('auth');
Route::get('/', [App\Http\Controllers\CoinController::class, 'index'])->name('index');
Route::get('/coin/{id}', [App\Http\Controllers\CoinController::class, 'show'])->name('coin');
Route::post('/add_coin', [App\Http\Controllers\CoinController::class, 'store'])->name('add_coin')->middleware('auth');

Route::get('/vote/{id}', [App\Http\Controllers\VoteController::class, 'create'])->name('vote')->middleware('auth');
Route::post('/add_vote', [App\Http\Controllers\VoteController::class, 'store'])->name('add_vote')->middleware('auth');