<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Coin;
use App\Models\Vote;

class VoteController extends Controller
{
    //
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
        $data = coin::find($id);
        return view('add_vote')->with("coin",$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $vote = new vote;
        $vote->userid = Request('userid');
        $vote->coinid = Request('coinid');
        if($vote->save()){
            return redirect('/');
        }

    }
}
