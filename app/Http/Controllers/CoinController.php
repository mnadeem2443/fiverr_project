<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Coin;
use App\Models\Vote;

class CoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = coin::all();
        $vote = vote::all();
        return view('welcome')->with("data",$data)->with("vote",$vote);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('add_coin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $coin = new coin;
        $coin->userid = Request('userid');
        $coin->coin_name = Request('coin_name');
        $coin->symbol = Request('symbol');
        $coin->logo = Request('logo');
        $coin->price = Request('price');
        $coin->market_cap = Request('market_cap');
        $coin->launch_date = Request('launch_date');
        $coin->binance_smart_chain = Request('binance_smart_chain');
        $coin->ethereum = Request('ethereum');
        $coin->solana = Request('solana');
        $coin->website = Request('website');
        $coin->telegram = Request('telegram');
        $coin->twitter = Request('twitter');
        $coin->additional = Request('additional');
        if($coin->save()){
            return view('add_coin')->with('OK',"OK");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = coin::find($id);
        return view('coin')->with("coin",$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
